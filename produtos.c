/**
 * Nome : Eduardo Miguel Ramos Borges
 * RGA  : 201211316033
 * Trabalho 4
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int id;
    char nome[100];
    float valor;
} Produtos;

/**
 * Insere produto no final do arquivo - Comando I
 *
 */
void insereProduto();

/**
 * Retorna a média dos valores de todos os produtos cadastrados - Comando M
 * 
 */
void mediaProdutos();

/**
 * Remove todos os produtos cadastrados - Comando R
 *
 */
void removeTodosProdutos();

/**
 * Remove todos os produtos com o valor igual ou abaixo do indicado - Comando D
 *
 */
void removePorPreco();

 /**
 * Imprime produto com codigo indicado - Comando P
 * 
 */
int imprimeProdutoPorCodigo(); 

/**
 * Exibe menu e retorna a opção desejada pelo usuário
 */
void menu();

int main() {
    menu();
}

void insereProduto() {
    FILE *arq = fopen("produtos.bin", "ab");
    int id;
    float valor;
    char nome;
    Produtos produto;

    if (arq != NULL) {
        printf("Digite o id do produto\n");
        scanf("%d", &produto.id);

        printf("Digite o nome do produto\n");
        scanf("%s", produto.nome);

        printf("Digite o valor do produto\n");
        scanf("%f", &produto.valor);

        fwrite(&produto, sizeof(Produtos), 1, arq);
        fclose(arq);    
        
        main();
    } else {
        printf("Arquivo não encontrado\n");
        main();
    }
}

void mediaProdutos() {
    FILE *arq = fopen("produtos.bin", "rb");
    int i = 0;
    Produtos produto;
    float valorTotal;

    if (arq == NULL) {
        printf("Arquivo não encontrado\n");
        main();
    } else {
        // Percorre arquivo e recuperando os preços de produtos
        while (fread(&produto, sizeof(Produtos), 1, arq)) {
            valorTotal += produto.valor;
            i++;
        }
        // Exibe média dos produtos cadastrados
        printf("A média de todos os produtos cadastrados é: %.2f\n", (valorTotal/i));
        main();
    }

}

void removeTodosProdutos() {
    int opt;
    printf("Essa operação removerá todos os produtos cadastrados, deseja prosseguir mesmo assim?\n");
    printf("1 - SIM\n");
    printf("2 - NÃO\n");
    scanf("%d", &opt);
    FILE *arq = fopen("produtos.bin", "rb");
    Produtos produto;
    int i = 0;

    switch (opt) {
        case 1:
        if (arq) {
            // Percorre arquivo para contar produtos cadastrados
            while (fread(&produto, sizeof(Produtos), 1, arq)) {
                i++;
            }
            fclose(arq);
            // remove arquivo e imprime quantos produtos foram deletados
            remove("produtos.bin");
            printf("%d produto(s) foram removidos\n", i);
            main();
        } else {
            printf("Arquivo não encontrado\n");
            main();
        }
            break;
        case 2:
            main();
            break;
        default:
            printf("Opção inválida\n");
            removeTodosProdutos();
            break;
    }
}

void removePorPreco() {
    FILE *arq = fopen("produtos.bin", "rb");
    FILE *novoArq = fopen("produtos_nao_removidos.bin", "ab");
    Produtos produto;
    float valor;
    printf("Digite o valor\n");
    scanf("%f", &valor);

    if (arq) {
        // Copia todos os produtos com o valor maior do que o passado para um novo arquivo
        while (fread(&produto, sizeof(Produtos), 1, arq)) {
            if (produto.valor > valor) {
                fwrite(&produto, sizeof(Produtos), 1, novoArq);
            }
        }
        // Remove o arquivo antigo e substitui pelo novo
        remove("produtos.bin");
        rename("produtos_nao_removidos.bin", "produtos.bin");
        fclose(novoArq);
        main();
    } else {
        printf("Arquivo não encontrado\n");
        main();
    }
}

int imprimeProdutoPorCodigo() {
    printf("Digite o código do produto\n");
    int id;
    scanf("%d", &id);

    // Declaração de arquivo
    FILE *arq = fopen("produtos.bin", "rb");

    // Declaração de estrutura
    Produtos produto;

    if (arq != NULL) {
        // percorre o arquivo e procura por registro com 
        while (fread(&produto, sizeof(Produtos), 1, arq)) {
            if (produto.id == id) {
                printf("Código : %d\n", produto.id);
                printf("Nome : %s\n", produto.nome);
                printf("Valor : %.2f\n", produto.valor);
                fclose(arq);

                return 1;
            }
        }

        printf("Produto não encontrado\n");
    } else {
        printf("Falha ao abrir arquivo\n");
    }

    return -1;
}

void menu() {
    printf("----------- - Menu - ---------------------------------------------------------\n");
    printf("Pressione I para inserir novo produto                                         \n");
    printf("Pressione M para a média dos valores de todos os produtos                     \n");
    printf("Pressione R para remover todos os produtos                                    \n");
    printf("Pressione D para remover todos os produtos com valor igual ou menos ao passado\n");
    printf("Pressione P para imprimir produto com o código desejado                       \n");
    printf("Pressione S para sair                                                         \n");
    printf("----------- - Menu - ---------------------------------------------------------\n");
    char opt;
    scanf(" %c", &opt);

    switch (opt) {
        case 'I':
            insereProduto();
            break;
        case 'P':
            imprimeProdutoPorCodigo();
            menu();
            break;
        case 'M':
            mediaProdutos();
            break;
        case 'R':
            removeTodosProdutos();
            break;
        case 'D':
            removePorPreco();
            break;
        case 'S':
            exit(-1);
            break;
        default :
            menu();
            break;
    }
}